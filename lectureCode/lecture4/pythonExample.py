class Foo:
    
    # Foo(num) creates a Foo
    # __init__: Integer -> Foo
    def __init__(self, num):
        self.num = num
        
    def __str__(self):
        return "This is a Foo with value " + str(self.num)

    # self == other
    # typically all fields are equal returns True
#    def __eq__(self, other):
#        return self.num == other.num
    

#    def __ne__(self, other):
#        return not self == other
    
    def helloWorld():
        return "Hello World"

f = Foo(10)
print( f == f )
#f.helloWorld() # method call
print(f)
