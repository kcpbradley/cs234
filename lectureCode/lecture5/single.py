
class Single:
    """
    Fields: _value stores any value
            _next stores the next node or None, if none
    """
    
    ## Single(value) produces a newly constructed singly-linked
    ##      node storing value
    ## __init__: Any -> Single
    def __init__(self, value, next = None):
        self._value = value
        self._next = next
    
    
    
class Multiset:
    ## Multiset() produces a newly constructed empty multiset
    ## __init__:   -> Multiset
    def __init__(self):
        self._head = None
    
    ## value in self produces True if value is an item in self
    ## __contains__: Multiset Any -> Bool
    def __contains__(self, value):
        curr = self._head
        while not curr is None:
            if curr._value == value:
                return True
            curr = curr._next
        return False
    
    ## self.add(value) adds value to self
    ## Effects: Mutates self by adding value to self
    ## add: Multiset Any -> None
    def add(self, value):
        self._head = Single(value, self._head)
    
    
    ## self.delete(value) removes an item with value from self
    ## Effects: Mutates self by removing an item with value
    ##      from self
    ## delete: Multiset Any -> None
    ## Requires: self contains an item with value
    def delete(self, value):
        if self._head._value == value:
            self._head = self._head._next
        else:
            prev = self._head
            curr = prev._next
            while not curr is None:
                if curr._value is value:
                    prev._next = curr._next
                    return
                prev = curr
                curr = curr._next
    
    def __str__(self):
        ret = "["
        curr = self._head
        if curr is None:
            ret ="[]"
            return ret
        ret += str(curr._value)
        curr = curr._next
        while not curr is None:
            ret += ", " + str(curr._value)
            curr = curr._next
        ret += "]"
        return ret
            
M = Multiset()
M.add(1)
print(M)
M.add(4)
M.add(7)
M.add(1)
M.add(1)
print(M)
M.delete(1)
print(M)